public class Complex {
    // 定义属性并生成getter,setter
    private double r;
    private double i;
    // 定义构造函数
    public Complex(double r,double i){
        this.r=r;
        this.i=i;
    }
    public static double getRealPart(double r){
        return r;
    }
    public static double getImagePart(double i){
        return i;
    }

    //Override Object
    public boolean equals(Object obj){

        Complex complex=(Complex) obj;
        if (complex.r!=r) {
            return false;
        }
        if(complex.i!=i){
            return false;
        }
        return true;
    }
    public String toString(){
        String str=new String();
        if (i==0) str=r+"";
        else if(i<0) str=r + ""+i+"i";
        else str=r+""+"+"+i+"i";
        return str;
    }

    // 定义公有方法:加减乘除
    Complex ComplexAdd(Complex a){
        return new Complex(r+a.r,i+a.i);
    }
    Complex ComplexSub(Complex a){
        return new Complex(r-a.r,i-a.i);
    }
    Complex ComplexMulti(Complex a){
        return new Complex(r*a.r-i*a.i,r*a.i+i*a.r);
    }
    Complex ComplexDiv(Complex a){
        return new Complex((r*a.r+i*a.i)/(a.r*a.r+a.i*a.i),(i*a.r-r*a.i)/(a.r*a.r+a.i*a.i));
    }
}