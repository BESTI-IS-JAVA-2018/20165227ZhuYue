import junit.framework.TestCase;
import org.junit.Test;

public class StringBufferDemoTest extends TestCase {
    StringBuffer a = new StringBuffer("zhuyueniupi");//（<=16）
    StringBuffer b = new StringBuffer("zhuyueniupizhuyueniupi");//（>16&&<=34）
    StringBuffer c = new StringBuffer("zhuyueniupizhuyueniupizhuyueniupi");//（>=34）
    @Test
    public void testcharAt() throws Exception{
        assertEquals('z',a.charAt(0));
        assertEquals('u',a.charAt(2));
        assertEquals('p',a.charAt(9));
        assertEquals('i',a.charAt(10));
    }
    @Test
    public void testcapacity() throws Exception{
        assertEquals(27,a.capacity());
        assertEquals(38,b.capacity());
        assertEquals(49,c.capacity());
    }
    @Test
    public void testlength() throws Exception{
        assertEquals(11,a.length());
        assertEquals(22,b.length());
        assertEquals(33,c.length());
    }
    @Test
    public void testindexOf() throws Exception{
        assertEquals(0,a.indexOf("zh"));
        assertEquals(3,a.indexOf("yueniu"));
        assertEquals(7,a.indexOf("iupi"));
    }
}
