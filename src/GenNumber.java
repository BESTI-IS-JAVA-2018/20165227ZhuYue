import java.io.*;
import java.util.Random;

public class GenNumber {
    public static void main(String[] args) {
        String filepath = System.getProperty("user.dir");
        filepath += "\\20165227.txt";
        System.out.println(filepath);

        try {
            File file = new File(filepath);
            if (!file.exists()) {   //如果不存在data.txt文件则创建
                file.createNewFile();
                System.out.println("20165227.txt创建完成");
            }
            FileWriter fw = new FileWriter(file);       //创建文件写入
            BufferedWriter bw = new BufferedWriter(fw);

            //产生随机数据，写入文件
            Random random = new Random();
            for (int i = 1; i < 227; i++) {
                int randint = (int) Math.floor((random.nextDouble() * 5227.0));
                bw.write(String.valueOf(randint));      //写入一个随机数
                bw.newLine();       //新的一行
            }
            bw.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}