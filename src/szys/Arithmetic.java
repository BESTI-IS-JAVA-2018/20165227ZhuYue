import java.util.Scanner;
import java.util.Random;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class Arithmetic {
    private static int level;
    private static int number;
    static Random generator = new Random();
    static int x = 0;
    static int answer;
    static double answer1;
    public static void main(String args[])throws IOException{
        cal A;
        A = new cal();
        sta rightrate;
        rightrate = new sta();
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入数字确认等级（1-4级）： ");
        level = scan.nextInt();
        System.out.println("请输入题目数量：");
        number = scan.nextInt();
        if(level == 1) {
            rightrate.right = 0;
            String[] operation = {"+", "-"};
            while (x != number) {
                A.a = generator.nextInt(100);
                A.b = generator.nextInt(100);
                A.op = generator.nextInt(2);
                System.out.printf("%d %s %d =", A.a, operation[A.op], A.b);
                answer = scan.nextInt();
                if (A.op == 0)
                    A.add(A.a,A.b);
                else if (A.op == 1)
                    A.sub(A.a,A.b);
                System.out.println("你的答案是：" + answer);
                if (answer == A.result1) {
                    System.out.println("正解！");
                    rightrate.right++;
                } else
                    System.out.println("错解！正确答案是" + A.result1);
                    x++;
            }
            rightrate.rate=(double) rightrate.right / number;
            rightrate.print();
        }
        else if(level == 2) {
            int key = 0;
            rightrate.right = 0;
            String[] operation = {"*", "/"};
            while (x != number) {
                A.a = generator.nextInt(51);
                A.b = generator.nextInt(51);
                A.op = generator.nextInt(2);
                System.out.printf("%d %s %d =", A.a, operation[A.op], A.b);
                if (A.op == 0) {
                    A.mul(A.a, A.b);
                    answer = scan.nextInt();
                    System.out.println("你的答案是：" + answer);
                    if (answer == A.result1) {
                        System.out.println("正解！");
                        rightrate.right++;
                    } else {
                        System.out.println("错解！正确答案是" + A.result1);
                    }
                    x++;
                }
                else if (A.op == 1) {
                    A.div(A.a, A.b);
                    answer1 = scan.nextDouble();
                    key = (int)(A.result2 * 100);
                    double key1 = key / 100.00;
                    if (answer == (int)key1*100/100|| answer1== key1||answer1==A.result2) {
                        System.out.println("正解！");
                        rightrate.right++;
                    }
                    else if (A.result2 % 1== 0.00)
                        System.out.println("错解！正确答案是1" + (int)key1*100/100);
                    else
                        System.out.println("错解！正确答案是2" + key1);
                    x++;
                }
            }
            rightrate.rate = (double) rightrate.right / number;
            rightrate.print();
        }
        else if(level == 3){
            int right = 0;
            double rate;
            String[] operation = {"+","-","*", "/"};
            RationalNumber key;
            key = null;
            while (x != number){
                int a, b, c, d;
                int op;
                a = generator.nextInt(10) + 1;
                b = generator.nextInt(10) + 1;
                c = generator.nextInt(10) + 1;
                d = generator.nextInt(10) + 1;
                op = generator.nextInt(4);
                RationalNumber r1 = new RationalNumber(a, b);
                RationalNumber r2 = new RationalNumber(c, d);
                System.out.printf( r1 + operation[op] + r2 + "=");
                InputStreamReader i=new InputStreamReader(System.in);
                BufferedReader j=new BufferedReader(i);
                String answer = j.readLine();
                if (op == 0)
                    key = r1.add(r2);
                else if (op == 1)
                    key = r1.subtract(r2);
                else if (op == 2)
                    key = r1.multiply(r2);
                else if (op == 3)
                    key = r1.divide(r2);
                String key1;
                key1 = key.toString();
                System.out.println("你的答案是： " + answer);
                if (key1.equals(answer)) {
                    System.out.println("正解！");
                    right++;
                }
                else
                    System.out.println("错解!正确答案是" + key1);
                x++;
            }
            rate =(double) right / number;
            System.out.println("检测结束，你的正确率是 " + rate);
        }
        else if(level == 4){
            Evaluator q = new Evaluator();
            InfixToSuffix w = new InfixToSuffix();
            int a, b, c, d, op1, op2, op3;
            int right = 0;
            double rate;
            String equation, suffix;
            String[] operation = {"+","-","*", "/"};
            while (x != number) {
               a = generator.nextInt(10);
               b = generator.nextInt(10);
               c = generator.nextInt(10);
               d = generator.nextInt(10);
               op1 = generator.nextInt(4) + 1;
               op2 = generator.nextInt(4) + 1;
               op3 = generator.nextInt(4) + 1;
               equation =  a + " " + operation[op1] + " " + b + " "  + operation[op2] + " " + c + " " + operation[op3] + " " + d;
               suffix = w.InfixToSuffix(equation);
               answer = q.evaluate(suffix);
               System.out.print(equation + " =");
               int answers = scan.nextInt();
               if (answers == answer) {
                   System.out.println("正解！");
                   right++;
               } else
                   System.out.println("错解");
               x++;
           }
            rate = (double)right / number;
            System.out.println("检测结束，你的正确率是 " + rate);
        }
    }
}
class cal {
    int op;
    int a,b;
    int result1;
    double result2;
    public int add(int a,int b) {
        result1 = a + b;
        return result1;
    }
    public int sub(int a,int b){
        result1 = a - b;
        return result1;
    }
    public int mul(int a,int b){
        result1 = a * b;
        return result1;
    }
    public double div(int a,int b){
        result2 = (double) a / b;
            result2=(int)(result2 * 100);
            result2=result2/100;
            return result2;
        }
    }
class sta{
    double rate;
    int right;
    public void print(){
        System.out.println("检测结束，你的正确率是：" + rate);
    }
}
