import junit.framework.TestResult;
import org.junit.Test;
import junit.framework.TestCase;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ArithmeticTest extends TestCase {
    @Test
    public void testadd(){
        cal A;
        A=new cal();
        assertEquals(5,A.add(2,3));
    }
    @Test
    public void testmul(){
        cal A;
        A=new cal();
        assertEquals(6,A.mul(2,3));
    }
    @Test
    public void testsub(){
        cal A;
        A=new cal();
        assertEquals(7,A.sub(9,2));
    }
    @Test
    public void testdiv(){
        cal A;
        A=new cal();
        assertEquals(2.33,A.div(7,3));
        assertEquals(5.0,A.div(35,7));
    }
    @Test
    public void testscore(){
        RationalNumber key;
        RationalNumber r1 = new RationalNumber(2, 1);
        RationalNumber r2 = new RationalNumber(3, 1);
        InputStreamReader i=new InputStreamReader(System.in);
        BufferedReader j=new BufferedReader(i);
        key = r1.add(r2);
        int key1;
        key1 = 0;
        assertEquals(0,key1);
    }
}