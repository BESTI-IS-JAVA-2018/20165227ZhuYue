import java.util.Stack;
import java.util.StringTokenizer;
public class InfixToSuffix {
    public String InfixToSuffix(String infix) {
        //输入需要转换的中缀表达式，输出后缀表达式
        Stack<String> stack = new Stack<String>();
        StringTokenizer tokenizer = new StringTokenizer(infix);
        String suffix = "";
        String temp;
        while (tokenizer.hasMoreTokens()) {
            String c = tokenizer.nextToken();
            switch (c) {
                case " ":
                    break;
                case "(":
                    stack.push(c);
                    break;
                case "+":
                case "-":
                    while (stack.size() != 0) {
                        temp = stack.pop();
                        if (temp == "(") {
                            stack.push("(");
                            break;
                        }
                        suffix += " " + temp;
                    }
                    stack.push(c);
                    suffix += " ";
                    break;
                // 碰到'*''/'，将栈中所有乘除运算符弹出，送到输出队列中
                case "*":
                case "/":
                    while (stack.size() != 0) {
                        temp = stack.pop();
                        if (temp == "(" || temp == "+" || temp == "-") {
                            stack.push(temp);
                            break;
                        } else {
                            suffix += " " + temp;
                        }
                    }
                    stack.push(c);
                    suffix += " ";
                    break;
                // 碰到右括号，将靠近栈顶的第一个左括号上面的运算符全部依次弹出，送至输出队列后，再丢弃左括号
                case ")":
                    while (stack.size() != 0) {
                        temp = stack.pop();
                        if (temp == "(")
                            break;
                        else
                            suffix += " " + temp;
                    }
                    break;
                //如果是数字，直接送至输出序列
                default:
                    suffix += c;
            }
        }
        while (stack.size() != 0) {
            suffix += " " + stack.pop();
        }
        return suffix;
    }
}