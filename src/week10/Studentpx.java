import java.util.*;
class StudentKey implements Comparable {
    double d=0;
    String s="";
    StudentKey (double d) {
        this.d=d;
    }
    StudentKey (String s) {
        this.s=s;
    }
    public int compareTo(Object b) {
        StudentKey st=(StudentKey)b;
        if((this.d-st.d)==0)
            return -1;
        else
            return (int)((this.d-st.d)*1000);
    }
}
class StudentIF {
    String name=null;
    int ID=0;
    double math,english,computer,total,aver;
    StudentIF(String s, double m, double e, double f, double a,double b,int c) {
        name=s;
        math=m;
        english=e;
        computer=f;
        total=a;
        aver=b;
        ID=c;
    }
}
public class Studentpx {
    public static void main(String args[]) {
        TreeMap<StudentKey, StudentIF> treemap = new TreeMap<StudentKey, StudentIF>();
        String str[] = {"王高源","刘香衫", "朱越", "苏祚堃", "赵凯杰"};
        int ID[] ={20165225,20165226,20165227,20165228,20165229};
        double math[] = {96, 77, 63, 89, 96};
        double english[] = {87, 70, 68, 86, 92};
        double computer[] = {97, 66, 73, 66, 91};
        double total[] = new double[5];
        double aver[] = new double[5];
        StudentIF student1[] = new StudentIF[5];
        StudentIF student2[] = new StudentIF[5];
        for (int k = 0; k < student1.length; k++) {
            total[k] = math[k] + english[k] + computer[k];
            aver[k] = total[k] / 3;
        }
        for (int k = 0; k < student1.length; k++) {
            student1[k] = new StudentIF(str[k], math[k], english[k], computer[k], total[k], aver[k], ID[k]);
        }
        StudentKey key[] = new StudentKey[5];
        student2= student1;
        for (int k = 0; k < key.length; k++) {
            key[k] = new StudentKey(student1[k].ID);
        }
        for (int k = 0; k < student1.length; k++) {
            treemap.put(key[k], student1[k]);
        }
        for (int k = 0; k < key.length; k++) {
            key[k] = new StudentKey(student1[k].total);
        }
        for (int k = 0; k < student1.length; k++) {
            treemap.put(key[k], student1[k]);
        }
        int number = treemap.size();
        System.out.println("自己和学号前后各两名学生共" + number + "个对象,按总成绩排序:");
        Collection<StudentIF> collection = treemap.values();
        Iterator<StudentIF> iter = collection.iterator();
        int i=0;
        while (iter.hasNext()) {
            StudentIF stu = iter.next();
            System.out.println(" 学号 " + stu.ID + " 姓名 " + stu.name + " 总成绩 " + stu.total);
            i++;
            if(i==5)
                System.out.println("自己和学号前后各两名学生共" + number + "个对象,按学号排序:");
        }
    }
}
